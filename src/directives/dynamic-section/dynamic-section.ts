import { Directive,
        Renderer2,
        Input,
        ViewContainerRef,
        ComponentRef,
        ComponentFactoryResolver
} from '@angular/core';

import { section } from '../../app/section';

import { HomeSectionComponent } from '../../components/home-section/home-section';

/**
 * Generated class for the DynamicSectionDirective directive.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/DirectiveMetadata-class.html
 * for more info on Angular Directives.
 */
@Directive({
  selector: 'dynamicSection',
})
export class DynamicSectionDirective {

  @Input() section: section;

  compRef: ComponentRef<any>;

  constructor(private el: ViewContainerRef,
              private ren: Renderer2,
              private resolver: ComponentFactoryResolver) {
    console.log('Hello DynamicSectionDirective Directive');
  }

  ngAfterViewInit() {
    this.loadComponent(this.section);
  }

  loadComponent(section: section) {
  console.log("load section id: ", section.id);
    let componentType = this.section.component;
    let componentFactory = this.resolver.resolveComponentFactory(HomeSectionComponent);
    this.el.clear();

    this.compRef = this.el.createComponent(componentFactory);
    (<HomeSectionComponent>this.compRef.instance).data = this.section;
  }

}
