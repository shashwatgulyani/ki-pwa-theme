import { Directive, ElementRef, Renderer2 } from '@angular/core';

/**
 * Generated class for the ScrollGradientDirective directive.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/DirectiveMetadata-class.html
 * for more info on Angular Directives.
 */
@Directive({
  selector: '[scrollGradient]',
  inputs: ['targetColor: targetColor']
})
export class ScrollGradientDirective {
  heightTimeout: any
  constructor(private el: ElementRef, private ren: Renderer2) {
    console.log('Hello ScrollGradientDirective Directive');
    console.log(this.el);
    // this.el.nativeElement.onresize(() => this.changeColor());
    this.heightTimeout = setInterval(this.changeColor(), 20);
  }

  //create a timeout, probably an observable
  //when there is change in height execute function

  changeColor() {
    console.log(this.el.nativeElement.clientHeight);
  }

  ngOnDestroy() {
    if(this.heightTimeout){
      clearInterval(this.heightTimeout);
    }
  }

}
