import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Calendar } from '@ionic-native/calendar';

import { HttpModule} from '@angular/http';

import { MyApp } from './app.component';
import { RootPage} from '../pages/root/root';
import { ExpandableHeaderComponent } from '../components/expandable-header/expandable-header';
import { SlideHeaderComponent } from '../components/slide-header/slide-header';
import { NavTabsComponent } from '../components/nav-tabs/nav-tabs';
import { SectionContentComponent } from '../components/section-content/section-content';
import { DataProvider } from '../providers/data/data';
import { SlideContainerComponent } from '../components/slide-container/slide-container';
import { ScrollGradientDirective } from '../directives/scroll-gradient/scroll-gradient';
import { HomeSectionComponent } from '../components/home-section/home-section';
import { DynamicSectionDirective } from '../directives/dynamic-section/dynamic-section';
import { EventSectionComponent } from '../components/event-section/event-section';
import { CommentSectionComponent } from '../components/comment-section/comment-section';
import { CommentsProvider } from '../providers/comments/comments';
import { CommentModalPageModule } from '../pages/comment-modal/comment-modal.module';
import { ParallaxHeader } from '../directives/parallax-header/parallax-header';

@NgModule({
  declarations: [
    MyApp,
    RootPage,
    ExpandableHeaderComponent,
    SlideHeaderComponent,
    NavTabsComponent,
    SectionContentComponent,
    SlideContainerComponent,
    ScrollGradientDirective,
    HomeSectionComponent,
    ParallaxHeader,
    DynamicSectionDirective,
    EventSectionComponent,
    CommentSectionComponent,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, {mode: 'md'}),
    HttpModule,
    CommentModalPageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    RootPage,
    HomeSectionComponent
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Calendar,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    DataProvider,
    CommentsProvider
  ]
})
export class AppModule {}
