export class comment {
  id: number;
  txt: string;
  img: number;
  guest_name: string;
  date_time: Date;
  status: number;
  is_owner: number;
}
