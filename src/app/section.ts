export class section {
  id: number;
  enabled: boolean;
  title: string;
  type: Object;
  text: any;
  imgs: Array<any>;
  slideImgsConfig: any;
  component: any;
}
