import { Component, EventEmitter, Renderer2, ViewChild, ElementRef } from '@angular/core';
import { NavController, Content } from 'ionic-angular';

import { DataProvider } from '../../providers/data/data';
import { section } from '../../app/section';

@Component({
  selector: 'page-root',
  templateUrl: 'root.html'
})
export class RootPage {

  currSection: section;
  sections: section[];
  startIndex: number;
  headerHeight: number;

  sliderUpdated: boolean = false;

  @ViewChild('myContent') myContent: Content;
  @ViewChild('mainContent', {read: ElementRef}) main: ElementRef;
  @ViewChild('navTabs', {read: ElementRef}) tabs: ElementRef;
  @ViewChild('headerImg', {read: ElementRef}) header: ElementRef;
  changeSectionSub: EventEmitter<any>;
  getDataSub: EventEmitter<any>;
  constructor(public navCtrl: NavController,
              public dataService: DataProvider,
              private renderer: Renderer2) {
  }

  ngOnInit() {
    this.getDataSub = this.dataService.getDataImportedEmitter().subscribe(res =>  {
      this.sections = res.sections;
      this.startIndex = this.findSectionIndex(res.defaultSection);
      this.currSection = this.sections[this.startIndex];
      console.log('currSection first update', this.currSection);
    });

    this.changeSectionSub = this.dataService.getChangeSectionEmitter()
                            .subscribe(section => {
                              console.log('section changed emit', section);
                              this.currSection = section;
                              if(section.type === 'no-header'){
                                this.scrollToMain();
                              }
                              else if(section.type === 'gallery'){
                                this.startGallery();
                              }
                            });
  }

  startGallery() {
    this.renderer.addClass(this.header.nativeElement, "full");
    this.renderer.setStyle(this.header.nativeElement, 'background', 'black url(https://ununsplash.imgix.net/photo-1421091242698-34f6ad7fc088?fit=crop&fm=jpg&h=650&q=75&w=950) center center/100% auto no-repeat');
  }

  scrollToMain() {
    this.renderer.removeClass(this.header.nativeElement, "full");

    let main = this.main.nativeElement.offsetTop;
    console.log(main);
    this.myContent.scrollTo(0, main, 800);
  }

  onSliderUpdate() {
    console.log("slider updated event fired");
    this.myContent.resize();
    this.sliderUpdated = true;
  }

  findSectionIndex(id: number): number {
    return this.sections.findIndex((section) => {
      return section.id === id;
    });
  }
//temporary fix, change this to when the image has loaded in header
  ngAfterViewInit() {
    setTimeout(() =>{
      this.myContent.resize();
      this.sliderUpdated = true;
    }, 2000)
  }


  ngOnDestroy() {
    this.getDataSub.unsubscribe();
    this.changeSectionSub.unsubscribe();
  }



}
