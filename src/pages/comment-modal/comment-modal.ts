import { Component, Renderer2 } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, ViewController } from 'ionic-angular';
import { URLSearchParams } from '@angular/http';
import { DataProvider } from '../../providers/data/data';
import { CommentsProvider } from '../../providers/comments/comments';
/**
 * Generated class for the CommentModalPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-comment-modal',
  templateUrl: 'comment-modal.html',
})
export class CommentModalPage {

  guestName: string;
  texts: any;
  imgPath: string;
  imgCodes: number[];
  currAvt: number;
  selectedEl: any;
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private dataSer: DataProvider,
              private comSer: CommentsProvider,
              private toastCtrl: ToastController,
              private viewCtrl: ViewController,
              private ren: Renderer2) {
    this.texts = this.navParams.get('texts');
    this.imgCodes = [1, 2, 3, 4, 5, 6];
    this.imgPath = "assets/images/";
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CommentModalPage');
  }
  ngOnInit() {
    this.guestName = this.dataSer.data.guestName;
  }

  selectAvatar(ev, imgCode: number) {
    if(this.selectedEl)
      this.removeHighlight(this.selectedEl);
    this.selectedEl = ev.target.offsetParent;
    this.highlight(this.selectedEl);
    this.currAvt = imgCode;
  }

  removeHighlight(el) {
    this.ren.removeClass(el, 'selected');
  }

  highlight(el) {
    this.ren.addClass(el, 'selected');
  }

  postComment(guestName:string, text: string) {
    if(!guestName){
      this.presentToast("Please provide your name")
    }
    else if(!text || text.length < 5){
      this.presentToast("Your message is too short (min-length: 5).");
    }
    else if(!this.currAvt){
      this.presentToast("Pleae select and Avatar.");
    }
    else {
      let urlSearchParams = new URLSearchParams();
      urlSearchParams.append('guest_name', guestName);
      urlSearchParams.append('comment_txt', text);
      urlSearchParams.append('action_id', '4');
      urlSearchParams.append('icon', this.currAvt+'');

      let body = urlSearchParams.toString();

      console.log(body);
      this.comSer.postComment(body).subscribe(res => {
        console.log(res);
        if(res.status){
          this.presentToast("Your message was added");
          this.dismiss();
        }
        else {
          this.presentToast("There was an error. Please Try again later.");
        }
      });
    }
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }


  generateImgSrc(imgCode: number): string {
    switch(+imgCode){
      case 1: return 'm1.png';
      case 2: return 'm2.png';
      case 3: return 'm3.png';
      case 4: return 'w1.png';
      case 5: return 'w2.png';
      case 6: return 'w3.png';
      default: return 'm3.png';
    }
  }

  presentToast(msg: string) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'top',
      showCloseButton: true
    });
    toast.present();
  }

}
