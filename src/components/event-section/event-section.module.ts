import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { EventSectionComponent } from './event-section';

@NgModule({
  declarations: [
    EventSectionComponent,
  ],
  imports: [
    IonicModule,
  ],
  exports: [
    EventSectionComponent
  ]
})
export class EventSectionComponentModule {}
