import { Component, Input } from '@angular/core';
import { Calendar } from '@ionic-native/calendar';
import { ToastController } from 'ionic-angular';
import { section } from '../../app/section';

import { DataProvider } from '../../providers/data/data';
/**
 * Generated class for the EventSectionComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'event-section',
  templateUrl: 'event-section.html'
})
export class EventSectionComponent {

  @Input() data: section;
  events: Array<any>;
  constructor(private dataService: DataProvider,
              private calendar: Calendar,
              private toastCtrl: ToastController) {
    console.log('Hello EventSectionComponent Component');
  }

  ngOnInit(){
    this.events = this.dataService.data.events;
    this.calendar.createCalendar('Ki-calendar').then(
      (msg) => { console.log(msg);},
      (err) => { console.log(err);}
    );
  }

  addToCalendar(ev){
    this.calendar.hasWritePermission().then((res) => {
      if(!res){
        this.calendar.requestWritePermission().then(() =>{
          this.createEvent(ev);
        })
      }
      else {
        this.createEvent(ev);
      }
    })
  }

  createEvent(ev){
    let startDate = new Date(ev.starts);
    let endDate = new Date(ev.ends);
    this.calendar.createEventInteractively(ev.name, ev.venue_add, ev.desc, startDate, endDate)
            .then(() => {
              this.presentToast("Added event to calendar");
            });
  }

  presentToast(msg: string) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom',
      showCloseButton: true
    });
    toast.present();
  }


}
