import { Component, Renderer2, Input, ElementRef, ViewChild } from '@angular/core';

import { section } from '../../app/section';
import { DataProvider } from '../../providers/data/data';
/**
 * Generated class for the NavTabsComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'nav-tabs',
  templateUrl: 'nav-tabs.html'
})
export class NavTabsComponent {

  sections: section[];
  @Input() currSection: section;

  selectedTab: any;

  @ViewChild('segmentTabs', {read: ElementRef}) segmentTabs: ElementRef;

  constructor(public dataService: DataProvider, private renderer: Renderer2) {
    console.log('Hello NavTabsComponent Component');
    this.sections = this.dataService.sections;
  }

  ngOnInit(){
  }

  ngOnChanges(changes) {
    if(changes.currSection){
      if( !changes.currSection.firstChange){
        console.log('nav-tabs changed', changes.currSection.currentValue, changes.currSection.previousValue);
        this.onSectionChanged(changes.currSection.currentValue);
      }
      else {
        //this.selectFirstTab();
      }
    }
  }

  ngAfterViewInit() {
    this.selectedTab = this.segmentTabs.nativeElement.children[0];
    this.selectDefaultTab();
  }

  selectDefaultTab() {
    this.renderer.addClass(this.selectedTab, 'segment-activated');
  }

  onSectionChanged(section: section) {
    this.renderer.removeClass(this.selectedTab, 'segment-activated');
    const selectedIndex = this.sections.map((section) => section.title).indexOf(section.title);
    this.selectedTab = this.segmentTabs.nativeElement.children[selectedIndex];
    this.renderer.addClass(this.selectedTab, 'segment-activated');
  }

  onSectionSelect(section) {
    if(this.currSection.id !== section.id)
      this.dataService.changeSection(section);
  }

  //select default tab on first load
}
