import { Component, Input, OnChanges, ViewChild, ViewChildren, QueryList } from '@angular/core';
import { Slides } from 'ionic-angular';
import { section } from '../../app/section';

import { DataProvider } from '../../providers/data/data';
/**
 * Generated class for the SectionContentComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'slide-container',
  templateUrl: 'slide-container.html'
})
export class SlideContainerComponent implements OnChanges{

  @ViewChild('contentSlider') contentSlider: Slides;

  @ViewChildren('sectionContent') sectionContent: QueryList<any>;

  @Input() currSection: section;
  @Input() currIndex: number;
  @Input() sections: section[];

  constructor(private dataService: DataProvider) {
    console.log('Hello SlideContainerComponent Component');
  }

  ngOnInit() {
  }

  onSlideChange(slider) {
    if(slider.getActiveIndex() <= this.sections.length -1){
      console.log("Slide Changed", slider.getActiveIndex());
      const sectionNow = this.sections[slider.getActiveIndex()];
      this.dataService.changeSection(sectionNow);
    }
  }

  ngOnChanges(changes) {
    if(changes.currSection && !changes.currSection.firstChange){
      console.log("changed currSection", changes.currSection);
      this.changeSlide(changes.currSection.currentValue);
    }
    if(changes.sections && changes.sections.firstChange){
      this.currSection = this.sections[this.currIndex];
      console.log('changed sections first time', changes.sections);
    }
  }

  ngAfterViewInit() {
    this.contentSlider.autoHeight = true;
  }

  getSectionIndex(target: section): number {
    return this.sections.findIndex((section) => {
      return section.id === target.id;
    });
  }

  changeSlide(section: section): void {
    let slideToIndex = this.getSectionIndex(section);
    this.contentSlider.slideTo(slideToIndex, 600, false);
    //don't run callbacks because that will trigger event to change
    //section on provider and root making it a loop.
  }


}
