import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { SlideHeaderComponent } from './slide-header';

@NgModule({
  declarations: [
    SlideHeaderComponent,
  ],
  imports: [
    IonicModule,
  ],
  exports: [
    SlideHeaderComponent
  ]
})
export class SlideHeaderComponentModule {}
