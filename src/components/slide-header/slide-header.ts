import { Component, Input, ViewChild, Output, EventEmitter, Renderer2 } from '@angular/core';
import { Slides } from 'ionic-angular';

import { section } from '../../app/section';
/**
 * Generated class for the SlideHeaderComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'slide-header',
  templateUrl: 'slide-header.html'
})
export class SlideHeaderComponent {
  @ViewChild('slideHeader') slideHeader: Slides;
  @Input() slides: section;

  @Output() sliderUpdated: EventEmitter<any> = new EventEmitter();

  slideConfig: any;
  constructor(private renderer: Renderer2) {
    console.log('Hello SlideHeaderComponent Component');
  }
  ngOnInit() {
    this.slideConfig = this.slides.slideImgsConfig;
  }

  ngAfterViewInit() {
    this.slideHeader.lockSwipes(false);
    this.slideHeader.autoHeight = true;
    console.log('this.slideHeader', this.slideHeader);
  }

  updatePage() {
    this.sliderUpdated.emit();
    this.slideHeader.resize();
  }

  ngOnChanges(changes){
    console.log('slide-header changed', changes.slides.currentValue, changes.slides.previousValue);
    setTimeout(() => this.updatePage(), 2000);
  }
}
