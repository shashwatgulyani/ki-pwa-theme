import { Component, Input, ElementRef, Renderer2, ViewContainerRef } from '@angular/core';
import { section } from '../../app/section';

import { DataProvider } from '../../providers/data/data';
/**
 * Generated class for the SlideContainerComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'section-content',
  templateUrl: 'section-content.html'
})
export class SectionContentComponent {

  @Input() content: section;
  isActive: boolean;

  constructor(private dataService: DataProvider,
              private el: ViewContainerRef,
              private ren: Renderer2) {
    console.log('Hello SectionContentComponent Component');
  }

  ngOnInit() {
    console.log(this.content);
    this.isActive = this.content.id === this.dataService.currSectionId;
    console.log("section active: ", this.isActive);
    this.injectComponents();

  }

  injectComponents(){
    if(this.content && this.content.type === 'home'){
      console.log("in inject components");
    }
  }


}
