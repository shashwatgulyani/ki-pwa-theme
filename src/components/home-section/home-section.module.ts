import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { HomeSectionComponent } from './home-section';

@NgModule({
  declarations: [
    HomeSectionComponent,
  ],
  imports: [
    IonicModule,
  ],
  exports: [
    HomeSectionComponent
  ]
})
export class HomeSectionComponentModule {}
