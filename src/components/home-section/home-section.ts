import { Component, Input } from '@angular/core';
import { section } from '../../app/section';

import { DataProvider } from '../../providers/data/data';
/**
 * Generated class for the HomeSectionComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'home-section',
  templateUrl: 'home-section.html'
})
export class HomeSectionComponent {

  @Input()
  data: section;

  constructor(private dataService: DataProvider) {
    console.log('Hello HomeSectionComponent Component');
    console.log(this.data);
  }

  ngOnInit() {
  }

  onItemSelect(selected){
    console.log(selected);
  }


}
