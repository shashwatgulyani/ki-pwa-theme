import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { CommentSectionComponent } from './comment-section';

@NgModule({
  declarations: [
    CommentSectionComponent,
  ],
  imports: [
    IonicModule,
  ],
  exports: [
    CommentSectionComponent
  ]
})
export class CommentSectionComponentModule {}
