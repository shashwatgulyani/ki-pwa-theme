import { Component, Input, ViewChild, ElementRef, Renderer2 } from '@angular/core';

import { ModalController } from 'ionic-angular';

import { CommentsProvider } from '../../providers/comments/comments';
import { CommentModalPage } from '../../pages/comment-modal/comment-modal';
import { section } from '../../app/section';
import { comment } from '../../app/comment';

/**
 * Generated class for the CommentSectionComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'comment-section',
  templateUrl: 'comment-section.html'
})
export class CommentSectionComponent {

  @ViewChild('commentInput') commentInput: ElementRef;

  @Input() data: section;
  comments: comment[];
  imagePath: string;
  imgSrc: string[];

  constructor(private commentService: CommentsProvider,
              private renderer: Renderer2,
              private modalCtrl: ModalController) {
    console.log('Hello CommentSectionComponent Component');
    this.imagePath = 'assets/images/'
  }

  ngOnInit() {
    this.commentService.getComments().subscribe(res => {
      if(+res.status){
        this.comments = res.data;
      }
      else {
        //show error
      }
    });
  }

  ngAfterViewInit() {
    console.log(this.commentInput);
  }

  openCommentModal() {
    let commentModal = this.modalCtrl.create(CommentModalPage, {texts: this.data.text}, {cssClass: 'page-comment-modal'});
    commentModal.present();
  }

  generateImgSrc(imgCode: number): string {
    switch(+imgCode){
      case 1: return 'm1.png';
      case 2: return 'm2.png';
      case 3: return 'm3.png';
      case 4: return 'w1.png';
      case 5: return 'w2.png';
      case 6: return 'w3.png';
      default: return 'm3.png';
    }
  }

}
