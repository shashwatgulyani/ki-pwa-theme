import { Component, Input, Output, EventEmitter, ViewChild, Renderer2, ElementRef } from '@angular/core';

import { section } from '../../app/section';

import { DataProvider } from '../../providers/data/data';

/**
 * Generated class for the ExpandableHeaderComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'expandable-header',
  templateUrl: 'expandable-header.html'
})
export class ExpandableHeaderComponent {

  @Input() scrollContent: any;
  newHeaderHeight: number;
  @Input()
  currSection: section;

  initialHeight: number;
  scrollHeader: boolean;
  @Input()
  sliderUpdated: boolean = false;
  headerEl: any;
  @Output()
  headerHeightChanged: EventEmitter<number> = new EventEmitter();

  constructor(private renderer: Renderer2,
              private element: ElementRef,
              private dataService: DataProvider)
  {
    console.log('Hello ExpandableHeaderComponent Component');
    this.headerEl = this.element.nativeElement;
  }

  getHeader() {
    console.log(this.currSection.type);
    if(this.currSection.type === 'home'){
      //make a autoplay of images
      this.renderer.setStyle(this.headerEl, 'height', '45vh');
      let imgs = this.dataService.data.imgs;
      this.scrollHeader = true;
      this.scrollContent.scrollToTop(600);
    }
    else if(this.currSection.type === 'gallery') {
      //set header size to 100% view port width
      let imgs = this.dataService.data.galleryImgs;
      this.renderer.setStyle(this.headerEl, 'height', '100vh');
      this.scrollContent.scrollToTop(600);
    }
    else if(this.currSection.type === 'no-header') {
      //set header height to 0
      this.renderer.setStyle(this.headerEl, 'height', '0');
      this.scrollContent.scrollToTop(600);
    }
  }

  count: number = 0;


  setBackground(){
    let bg = this.dataService.sections[0].imgs;
    let bgNo = (this.count % bg.length);
    console.log(bg[bgNo].src);
    // this.renderer.setStyle(this.headerEl, 'background', 'url('+bg[bgNo].src+') center center/cover no-repeat ');
    this.count++;
  }

  ngAfterViewInit() {
    if(this.sliderUpdated && this.scrollHeader)
      this.checkScroll();
  }

  ngAfterViewChecked() {
  }

  checkScroll() {
    this.initialHeight = this.headerEl.clientHeight;
    console.log(this.initialHeight);
    let scrollSub = false;
    if(!scrollSub){
      this.scrollContent.ionScroll.subscribe(ev => {
        scrollSub = true;
        return this.resizeHeader(ev);
      });
    }
  }

  ngOnChanges(changes){
    if(changes.currSection){
      this.getHeader();
    }
  }

  resizeHeader(ev): void {
    ev.domWrite(() => {
      if(ev.scrollTop < 0){
        this.renderer.setStyle(this.headerEl, 'height', '100vh');
      }
      else {
        this.newHeaderHeight = this.initialHeight - ev.scrollTop;
        if(this.newHeaderHeight < 0){
          this.newHeaderHeight = 0;
        }
        this.renderer.setStyle(this.headerEl, 'height', this.newHeaderHeight + 'px');
        this.headerHeightChanged.emit(this.newHeaderHeight);
        this.scrollContent.resize();
      }
    });
  }

}
