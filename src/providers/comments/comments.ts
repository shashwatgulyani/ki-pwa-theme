import { Injectable } from '@angular/core';
import { Http, Headers, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';

// import { comment } from '../../app/comment';

/*
  Generated class for the CommentsProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/


@Injectable()
export class CommentsProvider {
  commentUrl: string;
  postUrl: string;
  constructor(public http: Http) {
    console.log('Hello CommentsProvider Provider');
    this.commentUrl = 'assets/data/comments.json';
    this.postUrl = 'http://localhost:8080/klickinvite/panel/_/backend/invite_api/comments';

  }

  getComments(){
    return this.http.get(this.commentUrl).map(res => res.json());
  }

  postComment(comment) {
    const header= {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    let headers = new Headers(header);
    // headers.append('content-Type', 'application/x-www-form-urlencoded');
    return this.http.post(this.postUrl, comment , {headers: headers}).map(res => {
      console.log(res);
      return res.json()
    });
  }

}
