import { Injectable, Output, EventEmitter } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

import { section } from '../../app/section';
/*
  Generated class for the DataProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class DataProvider {

  data: any; // make a class of this possibly
  currSection: section;
  sections: section[];
  headerHeight: number;
  currSectionId: number;
  @Output() sectionChanged: EventEmitter<section> = new EventEmitter();
  @Output() dataImported: EventEmitter<any> = new EventEmitter();

  private dataUrl: string;

  constructor(public http: Http) {
    console.log('Hello DataProvider Provider');
    this.dataUrl = 'assets/data/data.json';
    this.onInit();
  }

  getData(): Observable<any> {
    return this.http.get(this.dataUrl).map(res => {
      console.log(res.json());
      return res.json() as section;
    });
  }
//make a class of data to ensure that data received is of
//the format the app expects

//put a check that the default section is set to one that is
//enabled

  getDataImportedEmitter(): EventEmitter<any> {
    return this.dataImported;
  }

  onInit() {
    this.sections = [];
    this.getData().subscribe(res => {
      this.currSectionId = res.defaultSection;
      this.currSection = res.sections[res.defaultSection];
      this.headerHeight = res.headerHeight;
      for(let section of res.sections){
        if(section.enabled){
          this.sections.push(section);
        }
      }
      this.data = {
        currSection: this.currSection,
        sections: this.sections,
        defaultSection: res.defaultSection,
        headerHeight: res.headerHeight,
        guestName: res.guest_name,
        guestId: res.guest_id,
        events: res.events
      };
      this.dataImported.emit(this.data);
    });
  }

  changeSection(section: section): void {
    this.currSection = section;
    this.currSectionId = section.id;
    this.sectionChanged.emit(section);
  }

  getChangeSectionEmitter(): EventEmitter<section> {
    return this.sectionChanged;
  }

}
